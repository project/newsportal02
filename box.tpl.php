<?php // $Id$
//
// Newsportal02 theme box.tpl.php file
//
?>
  <div class="box">
    <?php if ($title) { ?><h2 class="title"><?php print $title; ?></h2><?php } ?>
    <div class="content"><?php print $content; ?></div>
 </div>

