<?php // $Id$
//
// Newsportal02 theme template.php file
//


function newsportal_primary_links() {
  $counting = 0;
  $links = menu_primary_links();
  $result = count($links);
  if ($links) {
    $output .= '<ul id="navlist">';
    
    $counting = 0;
    foreach ($links as $link) {
    	$counting++;
      $link = l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment']);
      if ($counting == $result) {
       	/*$output .= '<li>' . $link . '1. counter: '.$counting.' result: '.$result.'</li>';*/
      	$output .= '<li>' . $link . '</li>';
      }
      else {
      	$output .= '<li>' . $link . ' | </li>';
      	/*$output .= '<li>' . $link . '2. counter: '.$counting.' result: '.$result.'</li>';*/
      } 
    }; 
    $output .= '</ul>';
  }
  return $output;
}

function newsportal_secondary_links() {
  $link = array();
  $output = '';
  if ($links = menu_secondary_links()) {
    foreach ($links as $link) {
      $output_array[] = l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment']);
    };
  $output = implode("&nbsp;|&nbsp;",$output_array);
  }
  return $output;
}